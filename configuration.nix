# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

## YshinKharu's NixOS Configuration
## Since 01-04-2022

## 06-17-2022
## Now easily reproducible on GitLab!
## $ curl https://gitlab.com/ysutils/nixos/-/raw/main/configuration.nix


## Just some manual partitioning stuff for myself.
# wipefs -a /dev/sda1
# wipefs -a /dev/sda2
# wipefs -a /dev/sda3
# mkfs.fat -F 32 /dev/sda1
# mkfs.ext4 /dev/sda2
# mkswap /dev/sda3
# swapon /dev/sda3
# mount /dev/sda2 /mnt
# mkdir /mnt/boot
# mount /dev/sda1 /mnt/boot
## ...where /dev/sda1 is /boot

# Use imports to spread things out if configuration.nix gets too big.
{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      # Other files can be included here.
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only

  networking.hostName = "nixi"; # Define your hostname.
  networking.wireless.enable = false;  # wpa_supplicant. Works in tandem with NetworkManager but excluded
  networking.networkmanager.enable = true; # Set to false if using wpa_supplicant instead
  
  # Both wpa_supplicant & NetworkManager can be used in unison.
  # NetworkManager ignores wpa_supplicant.
  # networking.networkmanager.unmanaged = [
  # "*" "except:type:wwan" "except:type:gsm"
  # ];

  # Set your time zone.
  time.timeZone = "America/Edmonton";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.wlo1.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
   console = {
     font = "Lat2-Terminus16";
     keyMap = "us";
   };

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Display managers.
  # Forces NixOS to not use LightDM
  # Automatic login is also disabled
  services.xserver.autorun = false;
  services.xserver.displayManager.lightdm.enable = false;
  services.xserver.displayManager.startx.enable = true;
  services.xserver.displayManager.autoLogin.enable = false;

  # Desktop environments.
  services.xserver.desktopManager.xfce.enable = true;

  # Window managers.
  services.xserver.windowManager.qtile.enable = true;
  services.xserver.windowManager.bspwm.enable = true;
  services.xserver.windowManager.ratpoison.enable = true;
  #services.xserver.windowManager.fluxbox.enable = true;
  #services.xserver.windowManager.dwm.enable = true; # Useless if the user already has a patched build
  
  # Enable OpenGL.
  hardware.opengl.enable = true;
  # Allow support for OpenGL.
  hardware.opengl.driSupport = true;
  # Allow x86 support for OpenGL on 64-bit.
  # Useful for compatibility layers or emulation, such as Wine.
  hardware.opengl.driSupport32Bit = true;

  # Manually specify graphics driver.
  services.xserver.videoDrivers = [ "intel" ];

  # Configure keymap in X11
  services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  # hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.dv = {
     isNormalUser = true;
     extraGroups = [ "wheel" "networkmanager" ]; # 'wheel' for sudo
   };

  # Allow packages with proprietary licenses
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  # NixOS will also install packages with proprietary licenses.
  environment.systemPackages = with pkgs; [
     #### NixOS defaults.
     vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
     wget
     firefox
     ## My packages.
     #### essentials
     tmux # Is not terminator
     #kitty
     #cool-retro-term
     #alacritty
     #terminator # Is not tmux
     htop
     xterm # Included if not installed
     #### wm things
     #rofi
     dmenu
     sxhkd
     brightnessctl
     lxqt.lxqt-powermanagement
     dunst
     nitrogen
     #tint2 # A lightweight system panel.
     #volumeicon
     #polybar # No longer used. Originally for my bspwm setup.
     #### networking
     networkmanager # REQUIRED !
     networkmanagerapplet # Needs networkmanager
     #### multimedia
     alsa-utils
     alsa-tools
     mpv-unwrapped
     #pulseaudio 
     #pavucontrol
     #### core
     #coreutils # Default is BusyBox
     xorg.xinit # REQUIRED !
     xorg.xorgserver # Installed if not already.
     doas # A more secure sudo
     super # Required for doas
     gvfs
     #dbus # Required for non-systemd
     xdg-user-dirs
     xdg-utils
     libGL
     xorg.libXi
     xorg.libX11
     libGLU # OpenGL implementation.
     gnome.file-roller # GNOME's archive manager
     unrar # For .rar support
     #xarchiver # I recommend file-roller.
     #### other cool stuff
     dejavu_fonts
     noto-fonts
     noto-fonts-emoji
     unicode-emoji
     feh # Image viewer.
     #elvis # An enhanced clone of vi
     neovim # A better maintained fork of Vim
     neofetch
   ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # Packages such as 'doas' require this to be enabled.
   programs.mtr.enable = true;
   programs.gnupg.agent = {
     enable = true;
     enableSSHSupport = true;
   };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?

}

